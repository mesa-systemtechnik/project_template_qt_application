var indexSectionsWithContent =
{
  0: "acmrsu~",
  1: "cmu",
  2: "u",
  3: "cmu",
  4: "acmrs~",
  5: "cms"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables"
};

var indexSectionLabels =
{
  0: "Alle",
  1: "Klassen",
  2: "Namensbereiche",
  3: "Dateien",
  4: "Funktionen",
  5: "Variablen"
};

