SOURCES += \
    ../src/calculator.cpp \
    ../src/mainwindow.cpp

HEADERS += \
    ../src/calculator.h \
    ../src/mainwindow.h

FORMS += \
    ../src/mainwindow.ui

TRANSLATIONS += \
    ../src/src_de_DE.ts
