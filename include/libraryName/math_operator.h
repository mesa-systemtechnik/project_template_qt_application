#ifndef MATH_OPERATOR_H
#define MATH_OPERATOR_H

#include "src_global.h"

class SRC_EXPORT Math_Operator
{
public:
  Math_Operator();
  int subtraction(int value1, int value2);
};

#endif // MATH_OPERATOR_H
