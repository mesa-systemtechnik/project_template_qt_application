#!/bin/bash
set -e

TMP_COVERAGE_INFO="report.info"
COVERAGE_INFO="coverage.info"
RESULTS_DIR="../coverage"
BUILD_TESTS_DIR="build/debug/tests"

cd $BUILD_TESTS_DIR

gcov main.gcno

lcov --directory . -c -o $TMP_COVERAGE_INFO
lcov --remove $TMP_COVERAGE_INFO -o $COVERAGE_INFO \
    '/usr/*' \
    'Qt*.framework*' \
    '*.h' \
    '*/tests/*'

genhtml $COVERAGE_INFO -o $RESULTS_DIR