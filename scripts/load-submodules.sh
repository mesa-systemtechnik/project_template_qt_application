#!/bin/bash

# Update all submodules from .gitmodules file
git submodule update

cd modules

DIRECTORIES="$(find . -maxdepth 1 -mindepth 1 -type d)"
BINARY_DIR="bin"
SHARED_HEADERS_DIR="src/sharedHeaders"
SRC_PRO_FILE_PATH="../src/src.pro"

# Loop trough all submodule directories
for dir in $DIRECTORIES;
do
    DIR_NAME="$(basename $dir)"

    # Copy all binary files from submodule project to 
    # application project's binary directory
    if [ -d $dir/$BINARY_DIR ]; then
        cp -r $dir/$BINARY_DIR/. ../bin
    fi

    # Copy all shared headers from submodule project to 
    # application project's include directory within submodule dir name
    if [ -d $dir/$SHARED_HEADERS_DIR ]; then
        cp -r $dir/$SHARED_HEADERS_DIR/. ../include/$DIR_NAME
    fi

    # Add LIBS and INCLUDEPATH String to src.pro file depend on library dir name
    if ! grep $DIR_NAME $SRC_PRO_FILE_PATH > /dev/null
    then
        sed -i "/LIBS/a \ \ \ \ \-l$DIR_NAME \\\ " $SRC_PRO_FILE_PATH
        sed -i "/INCLUDEPATH/a \ \ \ \ \$\$PWD/../include/$DIR_NAME \\\ " $SRC_PRO_FILE_PATH
    fi
done