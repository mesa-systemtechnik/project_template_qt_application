#ifndef TESTCASE_TWO_H
#define TESTCASE_TWO_H

#include "calculator.h"
#include <QObject>

class TestCase_Two : public QObject
{
    Q_OBJECT

  public:
    TestCase_Two();

  private slots:
    void test_case2();
    void test_addition();

    /**************************************************************
     *  following names are not test functions, but special
     *  functions automatically called to initialize
     *  and clean up your test
     * *************************************************************/
    void
    initTestCase(); // This function is called before the first test function
    void init();    // This function is called before each test function
    void cleanup(); // This function is called after each test function
    void cleanupTestCase(); // Function is called after the last test function
};

#endif // TESTCASE_TWO_H
