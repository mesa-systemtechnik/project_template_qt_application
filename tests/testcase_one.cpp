#include "testcase_one.h"
#include <QtTest>

TestCase_One::TestCase_One() {}

void TestCase_One::test_case1()
{
    int a = 1 + 2;

    QCOMPARE(a, 3);
}

void TestCase_One::initTestCase() {}
void TestCase_One::init() {}
void TestCase_One::cleanup() {}
void TestCase_One::cleanupTestCase() {}
