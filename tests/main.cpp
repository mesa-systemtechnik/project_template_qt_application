#include "testcase_one.h"
#include "testcase_two.h"
#include <QApplication>
#include <QtTest>
#include <map>
#include <QObject>
#include <memory>

using namespace std;

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  QStringList arguments = QCoreApplication::arguments();
  map<QString, unique_ptr<QObject>> tests;
  tests.emplace("testcase_one", make_unique<TestCase_One>());
  tests.emplace("testcase_two", make_unique<TestCase_Two>());

  if (arguments.size() >= 3 && arguments[1] == "-select") {
    QString testName = arguments[2];
    auto iter = tests.begin();
    while (iter != tests.end()) {
      if (iter->first != testName) {
        iter = tests.erase(iter);
      } else {
        ++iter;
      }
    }
    arguments.removeOne("-select");
    arguments.removeOne(testName);
  }

  int status = 0;
  for (auto &test : tests) {
    status |= QTest::qExec(test.second.get(), arguments);
  }

  return status;
}
