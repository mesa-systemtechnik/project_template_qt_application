#ifndef TESTCASE_ONE_H
#define TESTCASE_ONE_H

#include <QObject>

class TestCase_One : public QObject
{
    Q_OBJECT

  public:
    TestCase_One();

  private slots:
    void test_case1();

    /**************************************************************
     *  following names are not test functions, but special
     *  functions automatically called to initialize
     *  and clean up your test
     * *************************************************************/
    void
    initTestCase(); // This function is called before the first test function
    void init();    // This function is called before each test function
    void cleanup(); // This function is called after each test function
    void cleanupTestCase(); // Function is called after the last test function
};

#endif // TESTCASE_ONE_H
