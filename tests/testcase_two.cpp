#include "testcase_two.h"
#include <QtTest>

TestCase_Two::TestCase_Two() {}

void TestCase_Two::test_case2()
{
    int a = 2 + 3;

    QCOMPARE(a, 5);
}

void TestCase_Two::test_addition()
{
    double a = 2.3;
    double b = 4.2;

    Calculator calc;
    QCOMPARE(calc.addition(a, b), 6.5);
}

void TestCase_Two::initTestCase() {}
void TestCase_Two::init() {}
void TestCase_Two::cleanup() {}
void TestCase_Two::cleanupTestCase() {}
