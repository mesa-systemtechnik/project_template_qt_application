QT += testlib

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

include(../project_template_qt_application.pri)

INCLUDEPATH += ../src

QMAKE_CXXFLAGS += -g -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -fprofile-arcs -ftest-coverage  -O0

SOURCES +=  \
    main.cpp \
    testcase_one.cpp \
    testcase_two.cpp

HEADERS += \
    testcase_one.h \
    testcase_two.h

CONFIG(debug, debug|release){
    DESTDIR = $$PWD/../build/debug/tests
    OBJECTS_DIR = $$PWD/../build/debug/tests
    MOC_DIR = $$PWD/../build/debug/tests
    RCC_DIR = $$PWD/../build/debug/tests
}
