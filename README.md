# QT application boilerplate for desktop projects

A boilerplate for QT applications.

| Content             | Title         | Revision     |
| ------------------- |---------------| -------------|
| Framework           | Qt            | 5.14.1       |
| Unit test framwork  | Qt Test       | 5.14.1       |
| SQL Driver          | Sqlite        | 3.27.2       |

## Extern Library list

| Library       | Version       |
| ------------- |---------------| 
| libgit2       | v0.99         |

## Intern submodule list

| Library       | Version       |
| ------------- |---------------| 
| libraryName   | v1.0.0        |

## Getting started

### Load submodules

Load submodules, run from project root `./scripts/load-submodules.sh`.

### Use clang-format

ClangFormat describes a set of tools that are built on top of LibFormat. It can support your workflow in a variety of ways including a standalone tool and editor integrations.

Install `clang-format` and use the `.clang-format` file.

### Execute tests

Unit tests are created with QtTest Framework.

run tests with `make check` or use QtCreator GUI.

#### Run only selected testcase

Include testcase header and initialize in `tests/main.cpp`.
Emplace testcase with `tests.emplace("testcase_example", make_unique<TestCase_Two>());`.

run tests with parameter `-select` and the name of testcase, e.g. `-select testcase_example`.

### Generate code coverage

Code coverage is only executable on a Linux Distribution.

To generate code coverage it need the following packages installed:

* gvoc
* lcov
* genhtml

Make sure Application is compiled and all tests are executed.

run `./scripts/generate-coverage.sh` from project root directory.

### Generate documentation

Doxygen is a documentation generator, a tool for writing software reference documentation. The documentation is written within code, and is thus relatively easy to keep up to date. Doxygen can cross reference documentation and code, so that the reader of a document can easily refer to the actual code.

Install `doxygen` first and use the `Doxyfile` to generate the documentation.



