#ifndef CALCULATOR_H
#define CALCULATOR_H

/*!
 * \brief The Calculator class
 */
class Calculator
{
  public:
    Calculator();

    double addition(double value1, double value2);
};

#endif // CALCULATOR_H
