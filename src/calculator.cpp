#include "calculator.h"
#include <iostream>

using namespace std;

Calculator::Calculator() {}

/*!
 * \brief An addition function to add to numbers together
 * \param value1
 * \param value2
 * \return
 */
double Calculator::addition(double value1, double value2)
{
    double result = value1 + value2;

    cout << result << endl;

    return result;
}
