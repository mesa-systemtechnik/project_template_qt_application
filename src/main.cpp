#include "calculator.h"
#include "mainwindow.h"
#include "math_operator.h"

#include <QApplication>
#include <QFileInfo>
#include <QtDebug>
#include <QtSql>
#include <iostream>

using namespace std;

int main(int argc, char * argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../resource/db.sqlite");

    if (!db.open()) {
      qDebug() << "Database could not be opened!";
    }

    Calculator calc;
    calc.addition(2.4, 1.1);

    Math_Operator matho;
    cout << matho.subtraction(5, 1) << endl;

    return a.exec();
}
